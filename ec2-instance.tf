provider "aws" {
  region     = var.aws_region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}
resource "aws_security_group" "example" {
  name        = var.security_group_name
  description = var.security_group_description

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "example" {
  ami                    = var.instance_ami
  instance_type          = var.instance_type
  vpc_security_group_ids = [aws_security_group.example.id]
  key_name               = "terraform"

  root_block_device {
    volume_type           = "gp3"
    volume_size           = 25
    delete_on_termination = true
  }

  connection {
    type        = "ssh"
    user        = "ec2-user"
    private_key = var.newkey
    host        = self.public_ip
  }

  provisioner "remote-exec" {
    inline = [
      "sudo yum install docker -y",
      "docker --version"
    ]
  }

  tags = {
    Name = "ec2-instance"
  }
}
